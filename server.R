

server<-shinyServer(function(input, output,session) {
  
  ################################################################
  #############        filedata            #################
  ################################################################
  source("src/server_Filedata.R",encoding="UTF-8",local=TRUE)

  output$contents <- renderTable({
    filedata()
    
  })
  

  ############
  
  Prof <- reactive({
    df<-filedata()
    
    profil_2 <-
      df %>%
      # mutate(MNE=round(MNE,1)) %>%
      # mutate(MNT=round(MNT,1))%>%
      mutate(Verif_MNE_MNT=MNE<MNT)%>%
      mutate(Point_vise=NA)
    
    profil_2<- profil_2 %>%   mutate(MNE=ifelse(MNE<MNT,MNT,MNE)) %>%
      mutate(Verif_MNE_MNT=NULL)
    # recuperation des lignes portant sur les points d'observation
    pt_obs<-
      profil_2 %>%
      filter(Observateur == 1 )
    # recuperation du point de la cathedrale
    b<- profil_2 %>%
      slice(dim(profil_2)[1]) %>%
      mutate(MNT=300)
    
    
    
    for (i in 1: dim(pt_obs)[1]){
      
      #recuperation du point d'observation i
      a<- pt_obs %>%
        slice(i) %>%
        mutate(MNT=MNT+1.60)
      
      #creation d'un tableau avec le point d'observation i et le point de la cathedrale
      c<-union(a,b)
      
      reg_i=lm(data=c,MNT~X)
      
      for (j in 1:300){
        
        print(j)
        ancien=reg_i
        reg_i=lm(data=c,MNT~X)
        test<-profil_2 %>%
          filter(X>a$X) %>%
          mutate(MNE_calcul=reg_i$coefficients[1]+reg_i$coefficients[2]*X) %>%
          filter(MNE_calcul<=MNE)
        
        if (dim(test)[1]>1) {
          break}
        c<-c %>%   mutate(MNT = ifelse(is.na(Observateur),MNT-1,MNT))
      }
      
      if (j>1) {
        profil_2<-profil_2 %>%
          # CALCUL DE LA HAUTEUR DISPONIBLE PAR RAPPORT AU POINT D'OBSERVATION i
          mutate(Resultat1=ifelse(X>=a$X,reg_i$coefficients[1]+reg_i$coefficients[2]*X-MNT,NA)) %>%
          mutate(Resultat2=ifelse(X>=a$X,reg_i$coefficients[1]+reg_i$coefficients[2]*X,NA)) %>%
          mutate(Point_vise=ifelse(X==a$X,last(Resultat2),Point_vise)) 
        
        names(profil_2)[which(names(profil_2)=="Resultat1")]=paste("Hauteur_metre_dispo_Obs", i,sep="_")
        names(profil_2)[which(names(profil_2)=="Resultat2")]=paste("Hauteur_mNGF_dispo_Obs", i, sep="_")
      }
    }
    
    profil_2 <- profil_2 %>% mutate(Hauteur_metre_dispo =
                                      profil_2 %>%
                                      select(starts_with("Hauteur_metre_dispo_Obs")) %>%
                                      apply(MARGIN = 1, FUN = function(x) min(x,na.rm=TRUE)),
                                    Hauteur_mNGF_dispo =
                                      profil_2 %>%
                                      select(starts_with("Hauteur_mNGF_dispo_Obs")) %>%
                                      apply(MARGIN = 1, FUN = function(x) min(x,na.rm=TRUE)))

  })
  
  Prof_filtre <- eventReactive(input$calculate_mnGF,{
    Prof_filtre=Prof()
    Prof_filtre<-Prof_filtre %>%
      filter(X>=values_mNGF$v1 & X<=values_mNGF$v2) 
  })
  
  ######################################################################################
  #############        datad: table hauteurs dispo sur zoom du projet    ###############
  ######################################################################################
  source("src/server_datad.R",encoding="UTF-8",local=TRUE)
  
  
  
  # Affiche le tableau apres calcul des hauteurs disponibles sur le zoom du projet
  output$contents3 <- DT::renderDataTable(datad(),options = 
                                         list(pageLength =5,searching = FALSE))

  output$contents2 <-  DT::renderDataTable(Prof(),options = 
              list(pageLength = 20,searching = FALSE))
                                       

  
  ################################################################
  #############        GProfil_zoom            #################
  ################################################################
  source("src/server_Gprofil_zoom.R",encoding="UTF-8",local=TRUE)
  
  ################################################################
  #############        GProfil_mNGF            #################
  ################################################################
  source("src/server_Gprofil_mNGF.R",encoding="UTF-8",local=TRUE)

  ################################################################
  #############        GProfil_metre            #################
  ################################################################
  source("src/server_Gprofil_metre.R",encoding="UTF-8",local=TRUE) 

  
  #permet la bascule vers la partie graphiques calcul zone projet une fois avoir cliqué sur ""
  observeEvent(input$calculate_mnGF, {
    updateTabItems(session, "tabs", "GProfil_zoom")})
  
  values_mNGF <- reactiveValues()  
  observeEvent(input$Gprofil_mNGF_date_window,{
    value1 <- input$Gprofil_mNGF_date_window[[1]]
    value2 <- input$Gprofil_mNGF_date_window[[2]]
    values_mNGF$v1 <- (value1)
    values_mNGF$v2 <- (value2)
  })
  
  
  
  output$fichier_sortie = downloadHandler(    
    filename = function() {
    paste(file_name(),"_", Sys.Date(), ".csv", sep="")
  },
  content = function(file) {
    write.table(Prof(), file,sep="\t",row.names=FALSE)
  })
  
  output$fichier_sortie2 = downloadHandler(    
    filename = function() {
      paste("Partition_",file_name(),"_", Sys.Date(), ".csv", sep="")
    },
    content = function(file) {
      write.table(datad(), file,sep="\t",row.names=FALSE)
    })

})
