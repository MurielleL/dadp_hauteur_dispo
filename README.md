# Calcul de hauteurs disponibles

Travaux réalisés par

- DREAL Centre-Val de Loire - Murielle Lethrosne

# dadp_hauteur_dispo

*dadp_hauteur_dispo* est une application shiny permettant de visualiser l'intervisibilité, et le calcul de hauteurs disponibles par rapport à un profil MNE/MNT


## Cloner le projet

Dans Rstudio:
File/New Project/Version Control/Git

Repository URL: https://gitlab.com/MurielleL/dadp_hauteur_dispo.git
Project directory name: se remplira tout seul

Modifier le dossier si besoin.

Create project


